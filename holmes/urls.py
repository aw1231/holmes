from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^', include('grayscale.urls', namespace = 'grayscale')),
    url(r'^journal/', include('journal.urls', namespace = 'journal')),
    url(r'^admin/', include(admin.site.urls)),
    # Admin Views
    url(r'^orchestra/admin/',
        include(admin.site.urls)),

    # Registration Views
    # Eventually these will be auto-registered with the Orchestra URLs, but for
    # now we need to add them separately.
    url(r'^orchestra/accounts/',
        include('registration.backends.default.urls')),

# Logout then login is not available as a standard django
# registration route.
    url(r'^orchestra/accounts/logout_then_login/$',
        auth_views.logout_then_login,
        name='logout_then_login'),

    # Orchestra URLs
    url(r'^orchestra/',
        include('orchestra.urls', namespace='orchestra')),

# Beanstalk Dispatch URLs
    url(r'^beanstalk_dispatch/',
        include('beanstalk_dispatch.urls')),
]
