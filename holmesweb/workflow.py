from orchestra.workflow import Step
from orchestra.workflow import Workflow

def add_proposal

proposal_step = Step(
    slug='proposal',
    name='Add Proposal',
    description='Enter a new proposal',
    worker_type=Step.WorkerType.HUMAN,
    creation_depends_on=[],
    function=add_proposal,
)

rate_step = Step(
    slug='rate',
    name='Rate the experiment',
    description='Rate the experiment proposal',
    worker_type=Step.WorkerType.HUMAN,
    creation_depends_on=[crawl_step],
    required_certifications=[],
    review_policy={'policy': 'sampled_review'},
    user_interface={
        'javascript_includes': [
            '/static/simple_workflow/rate/js/modules.js',
            '/static/simple_workflow/rate/js/controllers.js',
            '/static/simple_workflow/rate/js/directives.js',
        ],
        'stylesheet_includes': [],
        'angular_module': 'simple_workflow.rate.module',
        'angular_directive': 'rate',
    }
)

holmes_workflow = Workflow(
    slug='holmes_workflow',
    name='Holmes Workflow',
    description='Create and rate a proposal'
)

holmes_workflow.add_step(proposal_step)
holmes_workflow.add_step(rate_step)
