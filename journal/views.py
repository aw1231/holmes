from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.utils import timezone
from .models import Choice, Question, Entry


def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'main.html', context)
def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'detail.html', {'question': question})

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'results.html', {'question': question})

def vote(request, question_id):
    p = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        if(request.POST['nc'] != ''):
            nc = addchoice(request, question_id)
            return nc
        # Redisplay the question voting form.
        return render(request, 'detail.html', {
            'question': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('journal:results', args=(p.id,)))
def addquestion(request):
    newquestion = request.POST['nq']
    nq = Question(question_text=newquestion,pub_date=timezone.now())
    nq.save()
    return HttpResponseRedirect(reverse('journal:index'))
def addchoice(request, q_id):
    q = get_object_or_404(Question, pk=q_id)
    newchoice = request.POST['nc']
    nc = Choice(choice_text=newchoice,votes=1,question_id=q_id)
    nc.save()
    return HttpResponseRedirect(reverse('journal:results', args=(q.id,)))
def entryview(request):
    latest_entry_list = Entry.objects.order_by('-pub_date')[:5]
    context = {'latest_entry_list': latest_entry_list}
    return render(request, 'entryview.html', context)
def addentry(request):
    newentry = request.POST['entry']
    ne = Entry(entry=newentry,pub_date=timezone.now())
    ne.save()
    return HttpResponseRedirect(reverse('journal:entryview'))